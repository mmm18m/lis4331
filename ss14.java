import java.util.InputMismatchException;
import java.util.Scanner;

public class ss14 {

    public static void main (String[] args){

        Scanner scanny = new Scanner(System.in);
        boolean repeatPrincipal = true, repeatRate = true, repeatYear = true;
        double principal = 0.0, rate  = 0.0;
        int years = 0;

        System.out.println("Program performs the following functions: ");
        System.out.println("1. Calculates amount of money saved for a period of years, at a specified interest rate");
        System.out.println("2. Returned amount is formatted in U.S. curerncy, and rounded to two decimal places");
        System.out.println("\n***Note: Program checks for non-numeric values as well as only int values for years");


        
       
       while (repeatPrincipal == true){
            try {
                System.out.println("Current prinicpal: $");
                principal = scanny.nextDouble();
                repeatPrincipal = false;
                
                
            } catch (InputMismatchException e ){
                System.out.println("Must be an number value");
                scanny.nextLine();
                repeatPrincipal = true;
            } 
       }

       while (repeatRate == true){
        try {
            System.out.println("Interest Rate: ");
            rate = scanny.nextDouble();
            repeatRate = false;
            
            
        } catch (InputMismatchException e ){
            System.out.println("Must be a number value");
            scanny.nextLine();
            repeatRate = true;
        } 
     }

     
     while (repeatYear == true){
        try {
            System.out.println("Years: ");
            years = scanny.nextInt();
            repeatYear = false;
            
            
        } catch (InputMismatchException e ){
            System.out.println("Must be an whole number value");
            scanny.nextLine();
            repeatYear = true;
        } 
     }
      
     System.out.println("You will have saved " + value + " in " + " years, at an interest rate of " + rate + " percent");


    }


}