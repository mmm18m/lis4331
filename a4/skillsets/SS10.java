import java.util.Scanner;
import java.util.InputMismatchException;
import java.text.DecimalFormat;

class SS10{
    public static void main(String args[]){
        double miles = 0.0, mph = 0.0;
        boolean repeat = true, repeatMPH = true;
        DecimalFormat df =new DecimalFormat("###.##");
        String endSelection;


        System.out.println("Program calculates approximate travel time");

        do {

        System.out.println("Please enter miles: ");
        while (repeat == true){
            try {
                Scanner scnr = new Scanner(System.in);
                miles = scnr.nextDouble();
                
                    while (miles <= 0 || miles >= 3000){
                    System.out.println("Miles must be more than 0 and less than 3000. Please re-enter");
                    miles = scnr.nextDouble();
                    }
                 repeat = false;
                
            } catch (InputMismatchException e){
                System.out.println("Please enter valid int");
        
            }
        }

        System.out.println("Please enter MPH: ");
        while (repeatMPH == true){
            try {
                Scanner scnr = new Scanner(System.in);
                mph = scnr.nextDouble();
                
                    while (mph < 0 || mph > 60){
                    System.out.println("Miles must be more than 0 and less than 60. Please re-enter");
                    mph = scnr.nextDouble();
                    }
                 repeatMPH = false;
                
            } catch (InputMismatchException e){
                System.out.println("Please enter valid MPH");
        
            }
        }

        double time = miles / mph;

        System.out.println("Predicted travel time is " + df.format(time) + " hour(s)");

        System.out.println("Go again? Y/N");
        Scanner scnr = new Scanner(System.in);
        endSelection = scnr.next();
        } while (endSelection.toUpperCase().equals("Y"));
    }//end main
}//end class