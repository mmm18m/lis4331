import java.util.Scanner;
import java.text.DecimalFormat;

class BookDemo
{
    public static void main(String[] args){

        Product product = new Product();
        Product product2 = new Product();
        Book book = new Book();
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("##.00");
        String code = "ABC123";
        String description = "Placeholder";
        double price = 0.0;


        System.out.println("\n/////Below are defaults/////");
        System.out.println(product.getCode());
        System.out.println(product.getDescription());
        System.out.println("$" + df.format(product.getPrice()));

        System.out.println("\n/////Below is user entered/////");
        System.out.print("Enter code: ");
        code = scanner.next();
        product.setCode(code);
        System.out.println("Enter description: ");
            description = scanner.next();
            product.setDescription(description);
        
        if (description != "Placeholder"){
            System.out.println("Enter price: ");
            price = scanner.nextDouble();
            product.setPrice(price);
        }

        System.out.println("\n/////Summon with product constructors/////");
        System.out.println(product.getCode());
        System.out.println(product.getDescription());
        System.out.println("$" + df.format(product.getPrice()));

        System.out.println("\n/////Setting p2 and printing to display literal values/////");
        product2.setCode("XYZ456");
        product2.setDescription("Widget");
        product2.setPrice(33.00);
        product2.print();


        System.out.println("\n/////Default Book Constructors/////");
        System.out.println(book.getCode());
        System.out.println(book.getDescription());
        System.out.println("$" + df.format(book.getPrice()));
        System.out.println(book.getAuthor());

        System.out.println("\n/////Override print()/////");
        book.print();

        System.out.println("\n/////Below is non-literal user entered values/////");
        System.out.print("Enter code: ");
        code = scanner.next();
        book.setCode(code);
        System.out.println("Enter description: ");
            description = scanner.next();
            book.setDescription(description);
        
        if (description != "Placeholder"){
            System.out.println("Enter price: ");
            price = scanner.nextDouble();
            book.setPrice(price);
        }
        System.out.print("Enter author: ");
        String author = scanner.next();
        book.setAuthor(author);


        System.out.println("\n/////print/////");
       System.out.println(book.getCode());
       System.out.println(book.getDescription());
       System.out.println(book.getPrice());
       System.out.println(book.getAuthor());
    }//end main



}//end ProductDemo