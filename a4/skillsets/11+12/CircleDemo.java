import java.util.Scanner;
import java.text.DecimalFormat;

class CircleDemo
{
    public static void main(String[] args){

        double radius = 0.0;
        Scanner scanner = new Scanner(System.in);
        DecimalFormat dcFormat = new DecimalFormat("##.00");


        Circle circle1 = new Circle();

        System.out.println("Enter radius:");
        radius = scanner.nextDouble();

        Circle circle2 = new Circle(radius);

        System.out.println("Circle 1 radius = " + dcFormat.format(circle1.getRadius()));
        System.out.println("Circle2 radius = " + dcFormat.format(circle2.getRadius()));

        System.out.println("Circle 1 diameter = " + dcFormat.format(circle1.diameter()));
        System.out.println("Circle 2 diameter = " + dcFormat.format(circle2.diameter()));

        System.out.println("Circle 1 circumference = " + dcFormat.format(circle1.circumference()));
        System.out.println("Circle 2 circumference = " + dcFormat.format(circle2.circumference()));

        System.out.println("Circle 1 area = " + dcFormat.format(circle1.area()));
        System.out.println("Circle 2 area = " + dcFormat.format(circle2.area()));
    }//end main



}//end CircleDemo