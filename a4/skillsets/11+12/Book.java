public class Book extends Product {

    private String author = "John Doe";



    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    private String code = "ABC123", description = "Placeholder Description";
    private double price = 0.0;

    public String getCode(){
        return code;
    }//end setCode

    public void setCode(String code){
        this.code = code;
    }//end setCode

    public String getDescription(){
        return description;
    }//end getDescription;

    public void setDescription(String description){
        this.description = description;
    }//end getDescription;

    public double getPrice(){
        return price;
    }//end getPrice

    public void setPrice(double price){
        this.price = price;
    }


    @Override
    public void print() {
        
        System.out.println("Inside Book Constructor");
        System.out.println("Code : " + code + "      Description: " + description + "      Price: " + price + "     Author: " + author);
    }

}//end class