import java.util.Scanner;
import java.text.DecimalFormat;

class ProductDemo
{
    public static void main(String[] args){

        Product product = new Product();
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("##.00");
        String code = "ABC123";
        String description = "Placeholder";
        double price = 0.0;


        System.out.println("\n/////Below are defaults/////");
        System.out.println(product.getCode());
        System.out.println(product.getDescription());
        System.out.println("$" + df.format(product.getPrice()));

        System.out.println("\n/////Below is user entered/////");
        System.out.print("Enter code: ");
        code = scanner.next();
        product.setCode(code);
        System.out.println("Enter description: ");
            description = scanner.next();
            product.setDescription(description);
        
        if (description != "Placeholder"){
            System.out.println("Enter price: ");
            price = scanner.nextDouble();
            product.setPrice(price);
        }

        System.out.println("\n/////Summon with product constructors/////");
        System.out.println(product.getCode());
        System.out.println(product.getDescription());
        System.out.println("$" + df.format(product.getPrice()));

    }//end main



}//end ProductDemo