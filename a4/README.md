# LIS4331

## Mary Meberg

### Assignment 4 Requirements:

1. Create interest application
2. Skillsets 10, 11, & 12
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of portfolio website
* Provide Skillsets

#### Mobile App Screenshots:

![Skillshot 1](img/app.gif)


#### Skillsets Screenshots

|  *Skillset 10*: | *Skillset 11*: | *Skillset 12*: |
|-----------------|----------------|---------------|
|![Skillshot 10](img/10.PNG)|![Skillshot 11](img/11.PNG)|![Skillshot 12](img/12.PNG)|

