# LIS4331

## Mary Meberg

### Assignment 3 Requirements:


1. Create a currency converting app using Android Studio
2. Skillsets 4, 5, & 6
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of mobile recipe app
* Provide Skillsets


#### Mobile App Screenshots:

![Skillshot 1](img/appGIf.gif)

#### Skillsets Screenshots


|  *Skillset 4*: | [*Skillset 5 (Click for Video)*:](https://youtu.be/-Pi8QejybBc) | [*Skillset 6 (Click for Video):*](https://youtu.be/jEWrk0ELalg) |
|-----------------|----------------|---------------|
|![Skillshot 1](img/timeConvert.PNG)|![Skillshot 2](img/evenOrOdd.png)|![Skillshot 3](img/paintcalc.png)|
