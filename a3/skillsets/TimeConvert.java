import java.util.InputMismatchException;
import java.io.PrintStream;

import java.util.Scanner;
class TimeConvert{
public static void main(String args[]){

int seconds = 0;
double days = 0, hours = 0, weeks = 0, years = 0, minutes = 0;
boolean repeat = true;

System.out.println("Program converts second to minutes, hours, days, weeks, and (regular) years--365 days.\nNotes:\n1. Use integer for seconds (must validate)\n2. Use printf() function to print\n3.Create Java \"constants\" for the following values:\n\tSECS_IN_MINS,\n\tMINS_IN_HR,\n\tHRS_IN_DAY\n\tDAYS_IN_WEEK\n\tDAYS_IN_YR");

System.out.println("\nPlease enter number of seconds: ");
while (repeat == true){
    try {
        Scanner scnr = new Scanner(System.in);
        seconds = scnr.nextInt();
        repeat = false;
    } catch (InputMismatchException e){
        System.out.println("Please enter valid int");

    }
}

minutes = seconds/60;
hours = minutes/60;
days = hours/24;
weeks = days/7;
years = days/365;

System.out.println("\n" + seconds + " second(s) equals...");

System.out.printf("\n%-5.2f minute(s)", minutes);
System.out.printf("\n%-5.3f hours(s)", hours);
System.out.printf("\n%-6.4f day(s)", days);
System.out.printf("\n%-7.5f week(s)", weeks);
System.out.printf("\n%-8.6f years(s)", years);





}//end main
}//end class