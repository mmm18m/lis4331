import javax.swing.*;
import java.util.InputMismatchException;
import java.awt.event.*; 
abstract class gui implements ActionListener {
    public static void main(String args[]){
       JFrame frame = new JFrame();
       JPanel panel = new JPanel();
       JLabel label = new JLabel("Enter Integer");
       JButton button = new JButton("Enter");
       JTextField integerEnter = new JTextField(20);
     
       

       panel.setBorder(BorderFactory.createEmptyBorder(30,30,10,10));
       panel.setLayout(null);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setSize(300,300);
       frame.setTitle("Even Or Odd");
       frame.add(panel);
       

       JOptionPane.showMessageDialog(frame,"Program uses Java GUI message and input dialogs. Program evaluates integers as even or odd. Note: Program does *not* perform data validation.");

      
       
       label.setBounds(10, 20, 80, 25);
       panel.add(label);
       integerEnter.setBounds(90, 75, 100, 25);
       panel.add(integerEnter);
       button.setBounds(100,125,75,25);
        panel.add(button);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
           
            try {
                int val = Integer.parseInt(integerEnter.getText()); 
                    if (val % 2 == 0){
                    JOptionPane.showMessageDialog(frame,"Even");
                } else if (val % 2 != 0) {
                    JOptionPane.showMessageDialog(frame,"Odd");
                }
            } catch (NumberFormatException exception){
                JOptionPane.showMessageDialog(frame,"Invalid entry. Enter an integer");

            }
        


            } //end Action

        }
        ); //end button
 

       frame.setVisible(true);
    }

}