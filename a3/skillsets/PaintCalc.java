import javax.swing.*;
import java.util.InputMismatchException;
import java.awt.event.*; 
import java.text.DecimalFormat;
import java.util.InputMismatchException;
abstract class PaintCalc implements ActionListener {
    public static void main(String args[]){
    
        String paintCostString = "", lengthString = "", widthString="", heightString="";
        double paintCost= 0.0, length= 0.0, width= 0.0, height= 0.0, area= 0.0, total= 0.0, gallons = 0.0;

        DecimalFormat moneyFormat = new DecimalFormat("$#,###,##0.00");
        DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");

        JOptionPane.showMessageDialog(null, "Program uses Java GUI message and input dialogs.\n Program determines paint cost per room \n For paint area simplicity: use length X height X 2 + weigth X height\n Format numbers as per below\n Program performs data validation"); 

        paintCostString = JOptionPane.showInputDialog(null, "Paint price per gallon:", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        while (paintCost == 0.0){
            try {paintCost = Double.parseDouble(paintCostString);}
            catch (NumberFormatException exception){
                JOptionPane.showMessageDialog(null, "Invalid entry. Enter an valid number");
                paintCostString = JOptionPane.showInputDialog(null, "Paint price per gallon:", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
            }
        }

        while (length == 0.0){
        lengthString = JOptionPane.showInputDialog(null, "Length", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        try {length = Double.parseDouble(lengthString);}
        catch (NumberFormatException exception){
            JOptionPane.showMessageDialog(null, "Invalid entry. Enter an valid number");
        }
    }

//Integer.parseInt(integerEnter.getText()); 
        while (width == 0.0){
        widthString = JOptionPane.showInputDialog(null, "Width", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        try {width = Double.parseDouble(widthString);}
        catch (NumberFormatException exception){
            JOptionPane.showMessageDialog(null, "Invalid entry. Enter an valid number");
        }
    }

    while (height == 0.0){
        heightString = JOptionPane.showInputDialog(null, "Height", "Paint Cost Calculator", JOptionPane.INFORMATION_MESSAGE);
        try {height = Double.parseDouble(heightString);}
        catch (NumberFormatException exception){
            JOptionPane.showMessageDialog(null, "Invalid entry. Enter an valid number");
        }
    }
        

        area = (length * height * 2) + (width * height * 2);
        gallons = area/350;
        total = gallons * paintCost;

        JOptionPane.showMessageDialog(null,"Paint = " + moneyFormat.format(paintCost) + " per gallon. \nArea of room = " + numberFormat.format(area) + " sq ft. \n Total = " + moneyFormat.format(total));
    }
}