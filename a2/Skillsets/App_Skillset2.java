import java.util.Scanner;

class App_Skillset2 {

    public static void main(String args[]){
    
        System.out.println("Program determines if first number is multiple of second number, prints result.");
        System.out.println("1) USe integers. 2)Use printf() function to print \nMust ONLY permit integer entry \n");

        int num1 = 0;
        int num2 = 0;
        boolean isMultiple = false;
        Scanner scnr = new Scanner(System.in);

        
    
        System.out.println("Enter num1:");
        while (num1 == 0){
       try {
        num1 = scnr.nextInt();
       } catch (Exception e){
        System.out.println("This entry is invalid");
        scnr.next(); }
       }

       System.out.println("Enter num2:");
       while (num2 == 0){
        try {
         num2 = scnr.nextInt();
        } catch (Exception e){
         System.out.println("This entry is invalid");
         scnr.next(); }
        }
 

        if (num1 % num2 == 0) {
            isMultiple = true;
            int num3 = num1/num2;
            System.out.println(num1 + " is a multiple of " + num2);
            System.out.println("The product of " + num3 + " and " + num2 + " is " + num1);
       }


    
    }//end main
}//end class