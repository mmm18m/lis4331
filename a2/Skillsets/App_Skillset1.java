import java.util.Scanner;

class App_Skillset1 {

    public static void main(String args[]){
    
    System.out.println("Non-OOP program calculates diameter, circumference and circle area.");
    System.out.println("Must use Java's built-in PI constant, printf(), and formatted to 2 decimal places");
    System.out.println("Must ONLY permit numeric entry \n");

    Scanner scnr = new Scanner(System.in);
    double radius = 0.0;

    System.out.println("Enter circle radius: ");
    while(!scnr.hasNextDouble()){
        System.out.println("Not valid number!\n");
        scnr.next();
        System.out.println("Please enter valid number: ");
    }//end while

    radius = scnr.nextDouble();
    System.out.printf("\n Circle diameter: %.2f\nCircumference: " + "%.2f\nArea: %.2f\n", (2*radius), (2*Math.PI*radius), (Math.PI * radius * radius)  );
    
    }//end main
}//end class