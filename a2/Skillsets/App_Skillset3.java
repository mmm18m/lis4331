import java.util.Scanner;

class App_Skillset3 {

    public static void main(String args[]){
    
    System.out.println("Author: Mary M. Meberg");
    System.out.println("Program counts, totals, and averages total number of user-entered scores");
    System.out.println("Please enter exam scores between 0 and 100, inclusive. \nEnter out of range number to end program");
    System.out.println("Must *only* permit numeric entry\n");

        double total = 0.0;
        int count = 0;
        double score = 0.0;
        double average = 0.0;
        Scanner scnr = new Scanner(System.in);

        while (score >= 0 && score <= 100){
            System.out.println("Enter exam score: ");

            while (!scnr.hasNextDouble()){
                System.out.println("Not valid number");
                scnr.next();
                System.out.println("Please try again. Enter exam score");
            }//end while

            score = scnr.nextDouble();

            if(score >= 0 && score <= 100){
                count = ++count;
                total = total + score;
            }//end if
        }//end while

        average = total/count;
        System.out.println("Count " + count);
        System.out.println("Total " + total);
        System.out.println("Average " + average);
    }//end main
}//end class