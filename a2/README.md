# LIS4331 - Advanced Mobile Web Application Development

## Mary Meberg

### Assignment 2 Requirements:

1. Create a tip
2. Skillsets 1, 2, & 3
3. Chapter Questions (Chs 3,4)

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of application
* Provide Skillsets

#### Mobile App Screenshots:

|  *Screenshot of Mobile App 1*: | *Screenshot of Mobile App 2*: | 
|--------------------------------------------------|-------------------------------------|
| ![Mobile App 1](img/home.PNG){ width=50%;}    | ![Mobile App 2](img/result.PNG)|

#### Skillsets Screenshots

|  *Skillset 1*: | *Skillset 2*: | *Skillset 3*: |
|-----------------|----------------|---------------|
|![Skillshot 1](img/skilllset1.PNG)|![Skillshot 2](img/skillset2.PNG)|![Skillshot 3](img/skilllset3.PNG)|
