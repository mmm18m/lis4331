# LIS4331 - Advanced Mobile Web Application Development

## Mary Marguerite Meberg

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/a1/README.md)
    * Install AMPPS
* Install JDK
* Install Android Studio and create My First App
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/a2/README.md)
	* Chapter Questions
* Complete "Bill" Application
	* Skillsets 1-3

3. [A3 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/a3/README.md)
	* Chapter Questions
* Complete "Convert Currency" Application
	* Skillsets 4-6

    
4. [A4 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/a4/README.md)
	* Chapter Questions
* Complete "Interest" Application
	* Skillsets 10-12

   

5. [A5 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/a5/README.md)
 

6. [P1 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/p1/README.md)
	* Chapter Questions
* Complete "Music" Application
	* Skillsets 7-9


7. [P2 README.md](https://bitbucket.org/mmm18m/lis4331/src/master/p2/README.md)
	* Chapter Questions
* Complete "Users" Application
