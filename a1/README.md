# LIS4331 - Adv. Mobile Web Application Development

## Mary Meberg

### **Assignment 1 Requirements:**

*Four Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)
4. Bitbucket repo links A: this assignment and B:completed tutorials.

#### **README.md file should include the following items:**

* Screenshot of running java Hello
* Screemshot of running Andriod Studio -My First App
* Screemshot of running Andriod Studio -Contacts App
* git commands with short description

#### Git commands w/short descriptions:

1. git init: Creates a new Git repository.
2. git status: Displays the state of the working directory and the staging area.
3. git add: Adds a change in the working directory to the staging area
4. git commit: Saves your changes to the local repository.
5. git push: Uploads local repository content to a remote repository.
6. git pull: Fetches and downloads content from a remote repository updates the local repository to match.
7. git branch: Lists all the branches in your repo, and tells you what branch you're currently in.

#### Assignment Screenshots:




| *Screenshot of running java Hello*: | *Screenshot of Android Studio - My First App*: |
|-------------------------------------|------------------------------------------------|
| ![JDK Installation Screenshot](img/jdk_installation.png) | ![Android Studio Installation Screenshot](img/Android_A1_Screenshot.PNG) |

    
| *Screenshot of Contacts App: Main*: | *Screenshot of Contacts App: Info*: |
|-------------------------------------|------------------------------------------------|
| ![JDK Installation Screenshot](img/exp_main.gif) | ![Android Studio Installation Screenshot](img/exp_contact.gif) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mmm18m/stationlocation/src/master/ "Bitbucket Station Locations")
