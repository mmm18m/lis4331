import java.util.InputMismatchException;
import java.io.PrintStream;

import java.util.Scanner;
class MeasurementConvert{
public static void main(String args[]){

int inches = 0;
double centimeters = 0, meters = 0, feet = 0, yards = 0, miles = 0;
boolean repeat = true;

System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles.\nNotes:\n1. Use integer for inches (must validate)\n2. Use printf() function to print\n3.Create Java \"constants\" for the following values:\n\tINCH_TO_CENTI,\n\tINCH_TO_METER,\n\tINCH_TO_FOOT,\n\tINCH_TO_YARD\n\tINCH_TO_MILE");

System.out.println("\nPlease enter number of inches: ");
while (repeat == true){
    try {
        Scanner scnr = new Scanner(System.in);
        inches = scnr.nextInt();
        repeat = false;
    } catch (InputMismatchException e){
        System.out.println("Please enter valid int");

    }
}

feet = inches / 12.0;
yards = feet / 3.0;
miles = feet / 5280.0;
meters = inches * 0.0254;
centimeters = meters * 100.0;

System.out.println("\n" + inches + " inch(es) equals...");

System.out.printf("\n%-2.6f centimeter(s)", centimeters);
System.out.printf("\n%-3.6f meter(s)", meters);
System.out.printf("\n%-6.6f feet", feet);
System.out.printf("\n%-6.6f yard(s)", yards);
System.out.printf("\n%-2.8f miles(s)", miles);







}//end main
}//end class