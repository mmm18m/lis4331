import javax.swing.*;
import java.util.InputMismatchException;
import java.awt.event.*; 
import java.text.DecimalFormat;
import java.util.InputMismatchException;
import javax.swing.*;
import javax.swing.border.*;
abstract class LegCalc implements ActionListener {
    public static void main(String args[]){
    
        
        JTextField TFLegA;
        JTextField TFLegB;
        JLabel lbLegA;
        JLabel lbLegB;
        JLabel lbLegC;
        JButton calcBtn;
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        lbLegC = new JLabel("Calculate for Leg C");
        lbLegA = new JLabel("Leg A: ");
        TFLegA = new JTextField(20);
        lbLegB = new JLabel("Leg B: ");
        TFLegB = new JTextField(20);

        calcBtn = new JButton("Calculate");

        panel.setBorder(BorderFactory.createEmptyBorder(30,30,10,10));
        panel.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        frame.setTitle("Skillset 8");
        frame.add(panel);

        lbLegA.setBounds(10, 20, 80, 25);
        panel.add(lbLegA);
        TFLegA.setBounds(60, 20, 100, 25);
        panel.add(TFLegA);

        lbLegB.setBounds(10, 40, 80, 25);
        panel.add(lbLegB);
        TFLegB.setBounds(60, 40, 100, 25);
        panel.add(TFLegB);

        lbLegC.setBounds(10, 60, 160, 25);
        panel.add(lbLegC);

        calcBtn.setBounds(10,100,180,25);
        panel.add(calcBtn);
     
     
     
            calcBtn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e){
               
                try {
                    int intA = Integer.parseInt(TFLegA.getText()); 
                    int intB = Integer.parseInt(TFLegB.getText());
                    int intC = 0;
              
                    intC = (intA * intA) + (intB * intB);
                    JOptionPane.showMessageDialog(frame,"C is " + intC);

                } catch (NumberFormatException exception){
                    JOptionPane.showMessageDialog(frame,"Invalid entry. Enter an integer");
    
                }
                
            }
                   
            });
           
        
            frame.setVisible(true);

    }//end main
      
    
}//end abstract class