import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;

public class MulitipleSelectionFrame extends JFrame
{
    
private JList burgerJList;
private JList copyJList;
private JButton copyJButton;
private static final String[] burgerNames = {
    "Mushroom", "Onion and Cheese", "Red Pepper and Bacon", "Italian", "Up Smacker", "Texan", "Californian", "Chili", "Bleu Cheese", "Fried Egg", "Triple Threat", "Veggie"
};
public MulitipleSelectionFrame(){
super("Multiple Selection Lists");
setLayout(new FlowLayout());
burgerJList = new Jlist<>(burgerNames);
burgerJList.setVisibleRowCount(5);
burgerJList.setFixedCellHeight(15);
burgerJList.setFixedCellWidth(140);
burgerJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

add(new JScrollPane(burgerJList));
copyJButton = new JButton("Copy >>>");
copyJButton.addActionListener(
    
new ActionListener()
{
public void actionPerformed(ActionEvent event){

copyJList.setListData(burgerJList.getSelectedValues());

}


});//end action listener

add( copyJButton);
copyJList = new JList();
copyburgerJList.setVisibleRowCount(5);
copyburgerJList.setFixedCellHeight(15);
copyburgerJList.setFixedCellWidth(140);
copyburgerJList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

add(new JScrollPane(copyJList)); };



}
//end class