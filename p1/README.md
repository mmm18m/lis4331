# LIS4331

## Mary Meberg

### Project 1 Requirements:

1. Create a app that uses MP3's using Android Studio
2. Skillsets 7, 8, & 9
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of "business card" app
* Provide Skillsets

#### Mobile App Screenshots:

[![Audi R8](img/APP.png)](https://www.youtube.com/watch?v=uftGzFU-WyU)
#### Skillsets Screenshots

|  *Skillset 7*: | *Skillset 8A*: | *Skillset 8B*: |
|-----------------|----------------|---------------|
|![Skillshot 7](img/skillset7.PNG)|![Skillshot 8](img/skillset8.PNG)|![Skillshot 9](img/skillset8A.PNG)|


|  *Skillset 9A*: | *Skillset 9B*: | 
|-----------------|----------------|
|![Skillshot 9](img/skillst7.PNG)|![Skillshot 9](img/skillst8.PNG)|