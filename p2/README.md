# LIS4331

## Mary Meberg

### Project 2 Requirements:

1. Create App
2. Chapter Questions


#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of application

#### Gif Of Running App:

 ![Website Home](img/ezgif-4-415f77ff10c5.gif)


#### Table of Images:

|  *Add Customer*: | *Update Customer*: | 
|-----------------|----------------|
|![Skillshot 7](img/addUser.PNG)|![Skillshot 8](img/updateUser.PNG)|

|  *View Customer*: | *Delete Customer*: | 
|-----------------|----------------|
|![Skillshot 7](img/viewUsser.PNG)|![Skillshot 8](img/deleteUser.PNG)|







